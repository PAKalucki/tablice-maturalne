package kalucki.przemyslaw.tablicematuralne

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.google.android.gms.ads.MobileAds
import com.google.gson.Gson
import kalucki.przemyslaw.tablicematuralne.Utils.showAsToastMessage
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity()
{

	override fun onCreate(savedInstanceState: Bundle?)
	{
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		setSupportActionBar(toolbar)

		MobileAds.initialize(this, "ca-app-pub-3940256099942544~3347511713")

		val navController = Navigation.findNavController(this, R.id.nav_host_fragment)
		NavigationUI.setupWithNavController(bottom_navigation, navController)

		//ca-app-pub-8686320119580377~7056911258
		//ca-app-pub-8686320119580377/6374384970
	}

	override fun onCreateOptionsMenu(menu: Menu): Boolean
	{
		menuInflater.inflate(R.menu.search_menu, menu)
		return true
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean
	{
		return when (item.itemId)
		{
			R.id.about ->
			{
				"About clicked".showAsToastMessage(applicationContext)
				true
			}
			else -> super.onOptionsItemSelected(item)
		}
	}

//	private fun createSearchItemsArray(): List<String>
//	{
//		val list = emptyList<String>()
//		list.zip(Gson().readJsonDataFromResourceFile(resources.openRawResource(R.raw.math_items)).map { item -> item.name })
//
//		return list
//	}
}
