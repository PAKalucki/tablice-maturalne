package kalucki.przemyslaw.tablicematuralne.Fragments


import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import kalucki.przemyslaw.tablicematuralne.ExpandableRecycler.*

import kalucki.przemyslaw.tablicematuralne.Item
import kalucki.przemyslaw.tablicematuralne.R
import kotlinx.android.synthetic.main.fragment_base.*
import java.util.ArrayList

abstract class BaseFragment : Fragment()
{
	private lateinit var adapter: GroupAdapter
	private lateinit var groups: List<ExpandableGroup<*>>
	abstract val resourceId: Int

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
	{
		return inflater.inflate(R.layout.fragment_base, container, false)
	}

	override fun onViewCreated(view: View, bundle: Bundle?)
	{
		super.onViewCreated(view, bundle)
		expandableRecyclerView.setItemViewCacheSize(3)
		adapter = GroupAdapter(groups)

		expandableRecyclerView.layoutManager = LinearLayoutManagerWithSmoothScroller(context!!)
		expandableRecyclerView.adapter = adapter
	}

	override fun onCreate(savedInstanceState: Bundle?)
	{
		super.onCreate(savedInstanceState)
		groups = setData()
	}

	override fun onViewStateRestored(bundle: Bundle?)
	{
		super.onViewStateRestored(bundle)
		if (bundle != null)
		{
			adapter.onRestoreInstanceState(bundle)
			adapter.notifyDataSetChanged()
		}
	}

	override fun onSaveInstanceState(savedInstanceState: Bundle)
	{
		super.onSaveInstanceState(savedInstanceState)
		adapter.onSaveInstanceState(savedInstanceState)
	}

	fun collapseAll()
	{
		adapter.collapseAllGroups()
	}

	fun collapseLast()
	{
		adapter.collapseLastGroup()
	}

	private fun setData(): List<ExpandableGroup<*>>
	{
		val list = ArrayList<ExpandableGroup<*>>()
		val items = readJsonDataFromFile(resourceId)


		for ((index, item) in items.withIndex())
		{
			if (index % 15 == 0)
			{
				list.add(Ad())
			}

			list.add(Group(item.name, listOf(Child(item.file))))
		}

		return list
	}

	//TODO optimize
	private fun readJsonDataFromFile(jsonResource: Int): List<Item>
	{
		val json = resources.openRawResource(jsonResource).use { inputStream ->
			inputStream.bufferedReader().readText()
		}
		return Gson().fromJson(json, object : TypeToken<List<Item>>() {}.type)
	}
}
