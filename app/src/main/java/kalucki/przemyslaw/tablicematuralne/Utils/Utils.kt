package kalucki.przemyslaw.tablicematuralne.Utils

import android.content.Context
import android.widget.Toast

inline fun Any.showAsToastMessage(context: Context)
{
	Toast.makeText(context, this.toString(), Toast.LENGTH_SHORT).show()
}