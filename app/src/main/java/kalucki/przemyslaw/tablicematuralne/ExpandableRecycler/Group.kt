package kalucki.przemyslaw.tablicematuralne.ExpandableRecycler

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup

class Group(title: String, items: List<Child>) : ExpandableGroup<Child>(title, items)
