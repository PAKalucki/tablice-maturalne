package kalucki.przemyslaw.tablicematuralne.ExpandableRecycler

import android.view.View
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.thoughtbot.expandablerecyclerview.listeners.OnGroupClickListener
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder
import kalucki.przemyslaw.tablicematuralne.R

class AdHolder(itemView: View) : GroupViewHolder(itemView)
{
	//TODO ads should pause when they are not visible and resume when they are
	fun loadAd()
	{
		val adView: AdView = itemView.findViewById(R.id.adView)
		val adRequest = AdRequest.Builder().build()
		adView.loadAd(adRequest)
	}

	override fun setOnGroupClickListener(listener: OnGroupClickListener)
	{
		// Do nothing
	}
}