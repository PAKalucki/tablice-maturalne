package kalucki.przemyslaw.tablicematuralne.ExpandableRecycler

import android.view.ViewGroup


import android.view.LayoutInflater.from
import com.thoughtbot.expandablerecyclerview.MultiTypeExpandableRecyclerViewAdapter
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder
import kalucki.przemyslaw.tablicematuralne.R
import java.lang.IllegalArgumentException

const val NORMAL_GROUP = 3
const val AD_GROUP = 4

class GroupAdapter(groups: List<ExpandableGroup<*>>) : MultiTypeExpandableRecyclerViewAdapter<GroupViewHolder, ChildHolder>(
		groups)
{
	override fun onCreateGroupViewHolder(parent: ViewGroup, viewType: Int): GroupViewHolder
	{
		return when (viewType)
		{
			NORMAL_GROUP -> GroupHolder(from(parent.context).inflate(R.layout.list_header, parent, false))
			AD_GROUP -> AdHolder(from(parent.context).inflate(R.layout.ad_container, parent, false))
			else -> throw IllegalArgumentException("Invalid view type")
		}
	}

	override fun onCreateChildViewHolder(parent: ViewGroup, viewType: Int): ChildHolder
	{
		val view = from(parent.context).inflate(R.layout.web_list_item, parent, false)
		return ChildHolder(view)
	}

	override fun onBindChildViewHolder(holder: ChildHolder, flatPosition: Int, group: ExpandableGroup<*>, childIndex: Int)
	{
		val child = (group as Group).items!![childIndex]
		holder.onBind(child)
	}

	override fun onBindGroupViewHolder(holder: GroupViewHolder, flatPosition: Int, group: ExpandableGroup<*>)
	{
		val viewType = getItemViewType(flatPosition)
		when (viewType)
		{
			NORMAL_GROUP -> (holder as GroupHolder).setTitle(group)
			AD_GROUP -> (holder as AdHolder).loadAd()
		}
	}

	override fun getGroupViewType(position: Int, group: ExpandableGroup<*>): Int
	{
		return when (group)
		{
			is Ad -> AD_GROUP
			else -> NORMAL_GROUP
		}
	}

	override fun isGroup(viewType: Int) = (viewType == NORMAL_GROUP || viewType == AD_GROUP)

	fun collapseAllGroups()
	{
		for (groupIndex in expandableList.expandedGroupIndexes.indices)
		{
			if (isGroupExpanded(groupIndex))
			{
				toggleGroup(getFlattenedGroupPosition(groupIndex))
			}
		}
	}

	fun collapseLastGroup()
	{
		var last = -1
		for (groupIndex in expandableList.expandedGroupIndexes.indices)
		{
			if (isGroupExpanded(groupIndex))
			{
				last = groupIndex
			}
		}
		if (last != -1)
		{
			toggleGroup(last)
		}
	}

	private fun getFlattenedGroupPosition(groupIndex: Int): Int
	{
		var runningTotal = 0
		for (i in 0 until groupIndex)
		{
			runningTotal += numberOfVisibleItemsInGroup(i)
		}
		return runningTotal
	}

	private fun numberOfVisibleItemsInGroup(group: Int): Int
	{
		return if (expandableList.expandedGroupIndexes[group])
		{
			expandableList.groups[group].itemCount + 1
		}
		else
		{
			1
		}
	}
}