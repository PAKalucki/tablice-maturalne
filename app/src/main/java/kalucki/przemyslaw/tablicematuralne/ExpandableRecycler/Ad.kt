package kalucki.przemyslaw.tablicematuralne.ExpandableRecycler

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup

class Ad(title: String = "", items: List<Child> = emptyList()) : ExpandableGroup<Child>(title, items)