package kalucki.przemyslaw.tablicematuralne.ExpandableRecycler

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Child(val content: String = ""): Parcelable