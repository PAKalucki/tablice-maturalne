package kalucki.przemyslaw.tablicematuralne.ExpandableRecycler

import android.view.View
import android.widget.TextView
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder
import kalucki.przemyslaw.tablicematuralne.R

class GroupHolder(itemView: View) : GroupViewHolder(itemView)
{
	fun setTitle(group: ExpandableGroup<*>)
	{
		val title: TextView = itemView.findViewById(R.id.tileTitle)
		title.text = group.title
	}
}
