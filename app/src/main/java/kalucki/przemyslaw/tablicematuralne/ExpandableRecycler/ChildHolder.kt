package kalucki.przemyslaw.tablicematuralne.ExpandableRecycler

import android.annotation.SuppressLint
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebView
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder
import kalucki.przemyslaw.tablicematuralne.R

@SuppressLint("SetJavaScriptEnabled")
class ChildHolder(itemView: View) : ChildViewHolder(itemView)
{
	private val web: WebView = itemView.findViewById(R.id.itemWeb)

	init
	{
		web.apply {
			settings.javaScriptEnabled = true
			settings.useWideViewPort = true
			settings.loadWithOverviewMode = true
			settings.cacheMode = WebSettings.LOAD_NO_CACHE
			settings.setSupportZoom(true)
			settings.builtInZoomControls = true
			settings.displayZoomControls = false
//			setLayerType(View.LAYER_TYPE_HARDWARE, null) java.lang.IllegalStateException: Unable to create layer for WebView
		}
	}

	fun onBind(child: Child)
	{
		web.loadUrl("file:///android_asset/" + child.content)
	}
}