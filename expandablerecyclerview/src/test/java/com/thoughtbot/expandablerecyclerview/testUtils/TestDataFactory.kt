package com.thoughtbot.expandablerecyclerview.testUtils

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import java.util.ArrayList
import java.util.Arrays

object TestDataFactory
{

	fun makeGroups(): List<ExpandableGroup<*>>
	{
		val list = ArrayList<ExpandableGroup<*>>()
		for (i in 0..5)
		{
			val items = Arrays.asList(i.toString() + ".0", i.toString() + ".1", i.toString() + ".2")
			list.add(ExpandableGroup("Section $i", items))
		}
		return list
	}

}
