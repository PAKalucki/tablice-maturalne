package com.thoughtbot.expandablerecyclerview

import android.content.Context
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import com.thoughtbot.expandablerecyclerview.models.ExpandableList
import com.thoughtbot.expandablerecyclerview.models.ExpandableListPosition.Companion.CHILD
import com.thoughtbot.expandablerecyclerview.models.ExpandableListPosition.Companion.GROUP
import com.thoughtbot.expandablerecyclerview.models.ExpandableListPosition.Companion.obtain
import com.thoughtbot.expandablerecyclerview.testUtils.TestDataFactory
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config

import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull

@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, sdk = [21])
class ExpandableListTest
{

	private var context: Context? = null
	private var groups: List<ExpandableGroup<*>>? = null

	@Before
	fun setUp()
	{
		MockitoAnnotations.initMocks(this)
		val application = RuntimeEnvironment.application
		assertNotNull(application)

		context = application
		groups = TestDataFactory.makeGroups()
	}

	@Test
	fun test_getVisibleItemCount()
	{
		val list = ExpandableList(groups!!)

		//initial state
		val initialExpected = 6
		val initialActual = list.visibleItemCount

		assertEquals(initialExpected, initialActual)

		//expand first group
		list.expandedGroupIndexes[0] = true

		//new state
		val newExpected = 9
		val newActual = list.visibleItemCount

		assertEquals(newExpected, newActual)
	}

	@Test
	fun test_getUnflattenedPosition()
	{
		val list = ExpandableList(groups!!)
		val flatPos = 3

		//initial state
		//flatPos 3 == group at index 3
		val initialExpected = obtain(GROUP, 3, -1, 3)
		val initialActual = list.getUnflattenedPosition(flatPos)

		assertEquals(initialExpected, initialActual)

		//expand first group
		list.expandedGroupIndexes[0] = true

		//flatPos 3 == child number 2 within group at index 0
		val newExpected = obtain(CHILD, 0, 2, 3)
		val newActual = list.getUnflattenedPosition(flatPos)

		assertEquals(newExpected, newActual)
	}
}
