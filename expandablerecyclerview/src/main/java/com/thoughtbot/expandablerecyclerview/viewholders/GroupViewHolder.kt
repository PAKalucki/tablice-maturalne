package com.thoughtbot.expandablerecyclerview.viewholders

import android.view.View
import android.view.View.OnClickListener
import com.thoughtbot.expandablerecyclerview.listeners.OnGroupClickListener
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup

import androidx.recyclerview.widget.RecyclerView

/**
 * ViewHolder for the [ExpandableGroup.title] in a [ExpandableGroup]
 *
 * The current implementation does now allow for sub [View] of the parent view to trigger
 * a collapse / expand. *Only* click events on the parent [View] will trigger a collapse or
 * expand
 */
abstract class GroupViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), OnClickListener
{

	private var listener: OnGroupClickListener? = null

	init
	{
		itemView.setOnClickListener(this)
	}

	override fun onClick(v: View)
	{
		if (listener != null)
		{
			listener!!.onGroupClick(adapterPosition)
		}
	}

	open fun setOnGroupClickListener(listener: OnGroupClickListener)
	{
		this.listener = listener
	}

	fun expand()
	{
	}

	fun collapse()
	{
	}
}
