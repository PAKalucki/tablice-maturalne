package com.thoughtbot.expandablerecyclerview.viewholders

import android.view.View
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup

import androidx.recyclerview.widget.RecyclerView

/**
 * ViewHolder for [ExpandableGroup.items]
 */
open class ChildViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
