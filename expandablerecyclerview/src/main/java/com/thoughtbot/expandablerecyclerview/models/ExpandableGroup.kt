package com.thoughtbot.expandablerecyclerview.models

import android.os.Parcel
import android.os.Parcelable

import java.util.ArrayList

/**
 * The backing data object for an [ExpandableGroup]
 */
open class ExpandableGroup<T>(val title: String, val items: List<T>?)
{

	val itemCount: Int
		get() = items?.size ?: 0
}
